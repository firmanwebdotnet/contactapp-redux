import {React, Component } from 'react'
import {NavLink} from 'react-router-dom'
import { connect } from 'react-redux'
import {getContactList} from '../actions'
import ContactTable from '../component/ContactTable'

class ContactList extends Component {
    constructor(props){
        super(props)
        this.returnContactList = this.returnContactList.bind(this);
    }

returnContactList() {
    return this.props.contactList;
}
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 mt-5">
                        <NavLink exact className="btn btn-info" to="/form-contact">Tambah Kontak</NavLink>
                        <ContactTable contactList={this.returnContactList()} />
                    </div>
                </div>
            </div>
        )
    }
}
// function mapStateToProps(state) {
//     return {
//       contactList : state.contacts.contactList,
//     }
//   }

function mapDispatchToProps(dispatch){
    return {
        getContactList: () => dispatch(getContactList())
    }
}

export default connect(mapDispatchToProps) (ContactList)