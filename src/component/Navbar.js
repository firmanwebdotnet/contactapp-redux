import {Component, React} from 'react'
import { NavLink } from 'react-router-dom'

class Navbar extends Component{
render(){
    return ( 
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <span className="navbar-brand">App Contact</span>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav">
                <li className="nav-item active">
                    <NavLink exact activeClassName="bg-danger" className="nav-link" to="/">Home</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink exact activeClassName="bg-danger" className="nav-link" to="/about">About</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink exact activeClassName="bg-danger" className="nav-link" to="/contact">Contact</NavLink>
                </li>
                </ul>
            </div>
            </nav>
        )
    }
}
export default Navbar
