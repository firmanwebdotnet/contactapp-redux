import React from 'react'

const ContactTable = (props) => {
    return (
        <table className="table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>No Tlp</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {
                props.contactList.map((contacts, index) =>{
                    return(
                        <tr key={index}>
                            <td>{contacts.nama}</td>
                            <td>{contacts.email}</td>
                            <td>{contacts.nohp}</td>
                            <td><button className="btn btn-danger" onClick={() => this.handleDelete(contacts.id)}>Hapus</button></td>
                        </tr>
                    )
                })              
            }
        </tbody>
    </table>
    )
}
export default ContactTable;