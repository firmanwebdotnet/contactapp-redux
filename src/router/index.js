import React from 'react'
import { Switch,Route } from 'react-router-dom'
import Home from '../views/Home'
import About from '../views/About'
import Contact from '../views/ContactList'
import Navbar from '../component/Navbar'
import FormContact from '../views/FormContact'

function Router(props){
    return (
        <Switch>
            <Route exact path="/">
                <Navbar />
                    <Home />
                </Route>
            <Route exact path="/about">
                <Navbar />
                    <About />
                </Route>
                <Route exact path="/contact">
                <Navbar />
                    <Contact />
                </Route>
                <Route path="/form-contact">
                    <Navbar />
                    <FormContact />
                </Route>
           
        </Switch>
        
    )
}

export default Router;
