import axios from 'axios';

export const addContact = () => {
    return {
        type: "ADD_CONTACT"
    }
}

export const handleInputChange = (name, value) =>{
    return {
        type: "HANDLE_INPUT_CHANGE",
        playload: {[name]: value }
    }
}

export const getContact = (json) => {
    return {
        type: "GET_CONTACTS",
        playload: json.contacts
    }
}

export const getContactList = () => {
    return (dispatch) => {
        dispatch(getContact());

        return axios
            .get('http://localhost:3000/listcontact')
            .then(response => {
                    dispatch(getContact(response.data))
            })
            .catch( err => {
                    dispatch(getContact(err))
            })

    }
}